# import the necessary packages
import cv2
from config import car_config as config
from MyModules import ImageToArrayPreprocessor
from MyModules import AspectAwarePreprocessor
from MyModules import MeanPreprocessor
from object_detection.utils import label_map_util
from car_mapping import Car_Mapping
from Car_Classification import car_classification
import numpy as np
import mxnet as mx
import pickle
import imutils
import os
import tensorflow as tf
import argparse

# construct the argument parse and parse the arguments
vArgPar = argparse.ArgumentParser()
vArgPar.add_argument("-m", "--model", required=True,help="base path for frozen checkpoint detection graph")
vArgPar.add_argument("-l", "--labels", required=True,help="labels file")
vArgPar.add_argument("-i", "--image", required=True,help="path to input image")
#vArgPar.add_argument("-ch", "--min-confidence", type=float, default=0.5)
vArgPar.add_argument("-c", "--checkpoints", required=True)
vArgPar.add_argument("-p", "--prefix", required=True)
args = vars(vArgPar.parse_args())


# load our pre-trained model
print("[INFO] loading pre-trained model...")
checkpointsPath = os.path.sep.join([args["checkpoints"],args["prefix"]])
vModel = mx.model.FeedForward.load(checkpointsPath,int(65))
vModel = mx.model.FeedForward(ctx=[mx.gpu(0)],symbol=vModel.symbol,arg_params=vModel.arg_params,aux_params=vModel.aux_params)
# initialize the image pre-processors
vAspAwPrep = AspectAwarePreprocessor(width=224, height=224)
vMeanPrep = MeanPreprocessor(config.R_MEAN, config.G_MEAN, config.B_MEAN)
vImgtArrPrep = ImageToArrayPreprocessor(dataFormat="channels_first")
vCar_Id = Car_Mapping()

# initialize the model
vSSD_Model = tf.Graph()

# create a context manager that makes this model the default one for
# execution
with vSSD_Model.as_default():
	# initialize the graph definition
	graphDef = tf.GraphDef()

	# load the graph from disk
	with tf.gfile.GFile(args["model"], "rb") as f:
		serializedGraph = f.read()
		graphDef.ParseFromString(serializedGraph)
		tf.import_graph_def(graphDef, name="")

# load the class labels from disk
labelMap = label_map_util.load_labelmap(args["labels"])
categories = label_map_util.convert_label_map_to_categories(labelMap, max_num_classes=int(2),use_display_name=True)
categoryIdx = label_map_util.create_category_index(categories)

# create a session to perform inference
with vSSD_Model.as_default():
	with tf.Session(graph=vSSD_Model) as sess:
		# grab a reference to the input image tensor and the boxes
		# tensor
		imageTensor = vSSD_Model.get_tensor_by_name("image_tensor:0")
		boxesTensor = vSSD_Model.get_tensor_by_name("detection_boxes:0")

		# for each bounding box we would like to know the score
		# (i.e., probability) and class label
		scoresTensor = vSSD_Model.get_tensor_by_name("detection_scores:0")
		classesTensor = vSSD_Model.get_tensor_by_name("detection_classes:0")
		numDetections = vSSD_Model.get_tensor_by_name("num_detections:0")

		# load the image from disk
		image = cv2.imread(args["image"])
		(H, W) = image.shape[:2]

		# check to see if we should resize along the width
		if W > H and W > 1000:
			image = imutils.resize(image, width=1000)

		# otherwise, check to see if we should resize along the
		# height
		elif H > W and H > 1000:
			image = imutils.resize(image, height=1000)

		# prepare the image for detection
		(H, W) = image.shape[:2]
		output = image.copy()
		image = cv2.cvtColor(image.copy(), cv2.COLOR_BGR2RGB)
		image = np.expand_dims(image, axis=0)

		# perform inference and compute the bounding boxes,
		# probabilities, and class labels
		(boxes, scores,l, N) = sess.run([boxesTensor, scoresTensor, classesTensor, numDetections],feed_dict={imageTensor: image})

		# squeeze the lists into a single dimension
		boxes = np.squeeze(boxes)
		scores = np.squeeze(scores)

		# loop over the bounding box predictions
		for (box, score) in zip(boxes, scores):
			# if the predicted probability is less than the minimum
			# confidence, ignore it
			if score < 0.2:
				continue

			# scale the bounding box from the range [0, 1] to [W, H]
			(startY, startX, endY, endX) = box
			startX = int(startX * W)
			startY = int(startY * H)
			endX = int(endX * W)
			endY = int(endY * H)
			(vPredCar, vPredPerc) = car_classification(output[startY:endY,startX:endX,:],vModel,vImgtArrPrep,vMeanPrep,vAspAwPrep,vCar_Id)
			# draw the prediction on the output image
            cv2.rectangle(output, (startX, startY), (endX, endY),(0,255,0), 2)
            cv2.putText(vImage, str(vPredCar), (startX - 10, startY), cv2.FONT_HERSHEY_SIMPLEX,0.6, (0, 0, 255), 2)
            cv2.putText(vImage, str(vPredPerc), (startX - 30, startY), cv2.FONT_HERSHEY_SIMPLEX,0.6, (0, 0, 255), 2)


		cv2.imshow("Output", output)
		cv2.waitKey(0)